<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
   "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html>
	<head>
		<title>Xima Library Engine Home Page</title>
		<link rel='stylesheet' href='css/main.css' type='text/css' />
		<link rel='stylesheet' href='css/search.css' type='text/css' />
		<link rel='stylesheet' href='css/toolbar.css' type='text/css' />
		<script src='js/jquery.js'></script>
		<script src='js/homepage.js'></script>
	</head>
	<body>
		<div id='toolbar'>
			<ul>
				<li><a href='operations/windows/borrowbook.php' id='borrowBTN'><img src='img/borrow.png'>Borrow Book</a></li>
				
				<li><a href='operations/windows/returnbook.php'><img src='img/return.png' id='returnBTN'>Return Book</a></li>
				
				<li><a href='#' class='floatright'><img src='img/help.png'>Help</a></li>
				
				<li><a href='#' class='floatright' id='addbookBTN'><img src='img/add.png'>Add Book</a></li>
				
				<li><a href='operations/windows/managebooks.php' class='floatright' id='managebooksBTN'><img src='img/book.png'>Manage Books</a></li>
				
				<li><a href='operations/windows/managestudents.php' class='floatright'><img src='img/user_edit.png'>Manage Students</a></li>
				
				<li><a href='#' class='floatright' id='addstudentBTN'><img src='img/user_add.png'>Add Student</a></li>
			</ul>
		</div>
		<div id='panel'>
			<br><br><br><br>
			<div id='search'>
				<h1>Search:</h1>
				<div id='searchcomponets'>
					<select name='selectcategory' id='selectcategory'>
						<option>Title</option>
						<option>Author</option>
					</select>
					<input type='text' value='Search for...' name='searchtxt' id='searchtxt' size='80'/>
				</div>
				<br>
			</div>
			<br><br>
			<div id='inpop'>
				
			</div>
			<div id='content'>
				<div id='version'>
					<h4>Your Version</h4>
					<p>Current Version: <?php
					$fileName = "version.txt";
					$file = fopen($fileName, 'r');
					$data = fread($file, 50000);
					fclose($file);
					echo $data;
					?></p>
					<p>Most Recent: 
					<?php
					$fileName = "http://celtrum.com/xima/nv.txt";
					$file = fopen($fileName, 'r');
					$data = fread($file, 45);
					fclose($file);
					echo $data;
					?>
					</p>
					<p><?php
					$current = "version.txt";
					$newest = "http://www.celtrum.com/xima/nv.txt";
					$file1 = fopen($current, 'r');
					$file2 = fopen($newest, 'r') or die("<font color='red'><b>No Internet Connection - cannot check for updates</b></font>");
					$newestVersionData = fread($file2, 45);
					$currentVersionData = fread($file1, 45);
					if($newestVersionData == $currentVersionData){
						echo "<img src='img/check.png'>Xima Library is up to date!";
					}
					else{
						echo "<img src='img/delete.png'>Xima Library is not up to date! Please update!<p><a href='http://celtrum.com/xima/release.zip'>Update</a>";
					}
					
					fclose($file1);
					fclose($file2);
					?></p>
					<h4>Version Changelog</h4>
					<?php
					echo "<textarea>";
					$fileName = "http://celtrum.com/xima/changelog.txt";
					$file = fopen($fileName, 'r');
					$data = fread($file, 50000);
					fclose($file);
					echo $data;
					echo "</textarea>";
					?>
				</div>
			</div>
		</div>
	</body>
</html>