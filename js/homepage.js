$(function(){
	var defaultText = "        Search for..."
	$('#searchtxt').css('color', 'gray');
	$('#search').hide().slideDown(800);
	$('#searchtxt').val(defaultText);
	$('#searchtxt').click(function(){
		var daVal = $(this).val();
		
		if(daVal == defaultText){
			$(this).css('color', 'black');
			$(this).val("");
		}
	});
	$('#searchtxt').blur(function(){
		var daVal = $(this).val();
		
		if (daVal == "") {
			$(this).css('color', 'gray');
			$(this).val(defaultText);
		}
	});
	$('#toolbar #addbookBTN').click(function(){
		var bla = window.open('operations/windows/addbook.php', 'addbookwin', 'height=550,width=400,scrollbars=yes', false);
	});
	$('#toolbar #addstudentBTN').click(function(){
		var bla = window.open('operations/windows/addstudent.php', 'addstudentwin', 'height=500,width=400,scrollbars=yes', false);
	});
});
