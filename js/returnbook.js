var initialBookValue = "Click on a book...";
var initialStudentValue = "Click on a student...";


function getFilterBook(){
    var searchValues = $('#filterbooktxt').val();
    var searchCat = $('#categorybook').val();
    $.ajax({
        url: '../../php/db/filterbooks.php',
        type: 'POST',
        data: 'searchVal=' + searchValues + '&searchCat=' + searchCat,
        
        success: function(result){
            $('#choosebook').html(result);
        }
    });
}

function getFilterStudent(){
    var searchValues = $('#filterstudenttxt').val();
    var searchCat = $('#categorystudent').val();
    $.ajax({
        url: '../../php/db/filterstudents.php',
        type: 'POST',
        data: 'searchVal=' + searchValues + '&searchCat=' + searchCat,
        
        success: function(result){
            $('#choosestudent').html(result);
        }
    });
}

function setBookID(uniqueID){
    $('#bookid').val(uniqueID);
	giveAlert("Book chosen!");
}

function setStudentID(studentID){
    $('#studentid').val(studentID);
	giveAlert("Student chosen!");
}
function giveAlert(alertText){
	$('#alertselection').fadeOut(400)
	.html(alertText)
	.slideDown(600, function(){
		$('#alertselection').slideUp(3000);
	});
}
$(function(){
    $('body').hide();
    $('body').fadeIn(800);
    $('#alertselection').hide();
	
    getFilterBook();
    getFilterStudent();
    
    $('#bookid').val(initialBookValue);
    $('#studentid').val(initialStudentValue);
    
    $('#filterbooktxt').keyup(function(){
        getFilterBook();
    });
    $('#filterstudenttxt').keyup(function(){
        getFilterStudent();
    });
	$('#categorybook').change(function(){
		getFilterBook();
	});
	$('#categorystudent').change(function(){
		getFilterStudent();
	});
    
    
    $('#submit').click(function(){
        var bookID = $('#bookid').val();
        var studentID = $('#studentid').val();
        if (bookID == initialBookValue || studentID == initialStudentValue) {
            alert("Click on both a book and student to successfully return a book.");
        }
        else {
            $.ajax({
                url: '../../php/db/returnbook.php',
                type: 'POST',
                data: 'bookID=' + bookID + '&studentID=' + studentID,
                
                success: function(result){
                    alert(result);
                    if (result == "The book was successfully returned.") {
                        $('#bookid').val(initialBookValue);
                        $('#studentid').val(initialStudentValue);
                        
                        $('#filterbooktxt').val("");
                        $('#filterstudenttxt').val("");
                    }
                    getFilterBook();
                    getFilterStudent();
                }
            });
        }
    });
});
