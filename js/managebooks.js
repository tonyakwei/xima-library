function getFilterBook(){
    var searchValues = $('#filtertxt').val();
    var searchCat = $('#searchcategory').val();
    $.ajax({
        url: '../../php/db/filterbooks.php',
        type: 'POST',
        data: 'searchVal=' + searchValues + '&searchCat=' + searchCat + '&showOperations=true',
        
        success: function(result){
            $('#booksresults').html(result);
        }
    });
}

function deleteBook(uniqueID){
	var askDelete = confirm("Are you sure you wish to delete this book?");
	if (askDelete) {
		$.ajax({
			url: '../../php/db/deletebook.php',
			type: 'POST',
			data: 'uniqueID=' + uniqueID,
			
			success: function(result){
				getFilterBook();
			}
		});
	}
}
function editBook(uniqueID){
	//$('#viewnotewrapper').fadeIn(800);
}
function viewNote(uniqueID){
	$('#viewnotetxt').val("Loading notes...");
	$.ajax({
		url: '../../php/db/getnotes.php',
		type: 'POST',
		data: 'dbtable=books&uniqueID='+uniqueID,
		
		success: function(result){
			if(result == ""){
				$('#viewnotetxt').val("There are no notes to display.");
			}
			else{
				$('#viewnotetxt').val(result);
			}
		}
	});
	$('#viewnotewrapper').fadeIn(800);
}
$(function(){
    $('body').hide();
    $('body').fadeIn(800);
    
	$('#viewnotewrapper').hide();
	
    getFilterBook();
    
    $('#filtertxt').keyup(function(){
        getFilterBook();
    });
    $('#searchcategory').change(function(){
        getFilterBook();
    });
	$('#viewnotewrapper input').click(function(){
		$('#viewnotewrapper').hide();
	});
});
