$(function(){
	$('body').hide();
	$('body').fadeIn(800);
	
	$(window).keydown(function(){
		$('#phpholder').fadeOut();
	});
	
	$('#submit').click(function(){
		var validationCheck = false;
		$('#phpholder').html("");
		$('#phpholder').show();
		var firstName = $('#firstname').val();
		var lastName = $('#lastname').val();
		var formNumber = $('#formnum').val();
		var formDivision = $('#formdiv').val();
		var notes = document.getElementById('notes').value;
		
		var alertBuffer = "";
		if(firstName == ""){
			alertBuffer += "-First name was not given\n";
		}
		if(lastName == ""){
			alertBuffer += "-Last name was not given\n";
		}
		if(formNumber == ""){
			alertBuffer += "-Form number was not given\n"
		}
		if(formDivision == ""){
			alertBuffer += "-Form division was not given\n";
		}
		if(alertBuffer != ""){
			alert("The following errors occured:\n\n"+alertBuffer);
			alertBuffer = "";
		}
		else{
			validationCheck = true;
		}
		
		if (validationCheck) {
			$.ajax({
				url: '../../php/db/addstudent.php',
				type: 'POST',
				data: 'firstName='+firstName+'&lastName='+lastName+'&formNumber='+formNumber+'&formDivision='+formDivision+'&notes='+notes,
				
				success: function(result){
					$('#firstname').val("");
					$('#lastname').val("");
					document.getElementById('notes').value = "";
					$('#phpholder').append("</p>" + result + "</p>");
				}
			});
		}
		return false;
	});
});
