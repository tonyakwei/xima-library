function getFilterStudent(){
    var searchValues = $('#filtertxt').val();
    var searchCat = $('#searchcategory').val();
    $.ajax({
        url: '../../php/db/filterstudents.php',
        type: 'POST',
        data: 'searchVal=' + searchValues + '&searchCat=' + searchCat + '&showOperations=true',
        
        success: function(result){
            $('#booksresults').html(result);
        }
    });
}
function deleteStudent(uniqueID){
	var askDelete = confirm("Are you sure you wish to delete this student?");
	if (askDelete) {
		$.ajax({
			url: '../../php/db/deletestudent.php',
			type: 'POST',
			data: 'uniqueID=' + uniqueID,
			
			success: function(result){
				getFilterStudent();
			}
		});
	}
}
function editStudent(uniqueID){
	$('#editbookwrapper').fadeIn(800);
}
function viewNote(uniqueID){
	$('#viewnotetxt').val("Loading notes...");
	$.ajax({
		url: '../../php/db/getnotes.php',
		type: 'POST',
		data: 'dbtable=students&uniqueID='+uniqueID,
		
		success: function(result){
			if(result == ""){
				$('#viewnotetxt').val("There are no notes to display.");
			}
			else{
				$('#viewnotetxt').val(result);
			}
		}
	});
	$('#viewnotewrapper').fadeIn(800);
}

$(function(){
    $('body').hide();
    $('body').fadeIn(800);
    
	$('#viewnotewrapper').hide();
	$('#editobjectwrapper').hide();
	$('#shadelayer').hide();
	//$('#editobjectwrapper').fadeIn(800);
	
    getFilterStudent();
    
	$('#submit').click(function(){
		if (1) {
			$.ajax({
				url: '../../php/db/getobject.php',
				type: 'POST',
				data: 'db=' + db + '&uniqueID=' + uniqueID + '&dataCategory=' + objectToGet,
				
				success: function(result){
					alert(result);
				}
			});
		}
		return false;
	});
	
    $('#filtertxt').keyup(function(){
        getFilterStudent();
    });
    $('#searchcategory').change(function(){
        getFilterStudent();
    });
	$('#viewnotewrapper input').click(function(){
		$('#viewnotewrapper').hide();
	});
});
