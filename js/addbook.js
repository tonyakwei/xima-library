$(function(){
	$('body').hide();
	$('body').fadeIn(800);
	
	$(window).keydown(function(){
		$('#phpholder').fadeOut();
	});
	
	$('#submit').click(function(){
		var validationCheck = false;
		$('#phpholder').html("");
		$('#phpholder').show();
		var title = $('#title').val();
		var author = $('#author').val();
		var status = $('#status').val();
		var quality = $('#quality').val();
		var genre1 = $('#genre1').val();
		var genre2 = $('#genre2').val();
		var dew1 = $('#dew1').val();
		var dew2 = $('#dew2').val();
		var isbn = $('#isbn').val();
		var pages = $('#pages').val();
		var notes = document.getElementById('notes').value;
		
		if(genre1 == ""){
			genre1 = "Not provided";
		}
		if(genre2 == ""){
			genre2 = "Not provided";
		}
		
		//Checking for form validation
		var alertBuffer = "";
		if(title == ""){
			alertBuffer += "-Title was not specified\n";
		}
		if(author == ""){
			alertBuffer += "-The author was not specified\n";
		}
		if(status == ""){
			alertBuffer += "-Status of location of book was not specified\n";
		}
		if(quality == ""){
			alertBuffer += "-Quality of book was not specified\n";
		}
		if(pages != undefined && isNaN(pages)){
			alertBuffer += "-Pages is not a number. To not specify the number of pages, leave it blank\n"
		}
		if(alertBuffer != ""){
			alert("The following errors occured:\n\n"+alertBuffer);
			alertBuffer = "";
		}
		else{
			validationCheck = true;
		}
		
		if (validationCheck) {
			$.ajax({
				url: '../../php/db/addbook.php',
				type: 'POST',
				data: 'title=' + title + '&author=' + author + '&status=' + status + '&quality=' + quality + '&genre1=' + genre1 + '&genre2=' + genre2 +'&dew1=' + dew1 + '&dew2=' + dew2 + '&isbn=' +isbn+ '&pages=' + pages + '&notes=' + notes,
				
				success: function(result){
					$('#title').val("");
					$('#author').val("");
					$('#status').val("IN");
					$('#quality').val("");
					$('#pages').val("");
					document.getElementById('notes').value = "";
					$('#phpholder').append("</p>" + result + "</p>");
				}
			});
		}
		return false;
	});
});
