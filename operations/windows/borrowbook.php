<html>
    <head>
        <title>Borrow book...</title>
        <link rel='stylesheet' href='../../css/borrowbook.css' type='text/css' />
		<link rel='stylesheet' href='../../css/toolbar.css' type='text/css' />
		<link rel='stylesheet' href='../../css/tables/skyblue.css' type='text/css' />
		<script src='../../js/jquery.js'></script>
		<script src='../../js/borrowbook.js'></script>
    </head>
    <body>
    	<div id='alertselection'>
    			
		</div>
    	<div id='toolbar'>
    		<ul>
    			<li><a href='../../index.php'><img src='../../img/home.png'>Home</a></li>
    		</ul>
    	</div>
        <h1>Borrow book...</h1>
        <div id='borrowdiv'>
            <h2>Choose book...</h2>
            <select id='categorybook'>
                <option>TITLE</option>
                <option>AUTHOR</option>
                <option>UNIQUE_ID</option>
            </select>
            <label for='bookid'>
                Book ID:
            </label>
            <input type='text' size='15' id='bookid' disabled='false'/><input type='text' id='filterbooktxt'>
            <div id='choosebook'>
            </div>
            <h2>Choose student...</h2>
            <select id='categorystudent'>
                <option>FIRST_NAME</option>
                <option>LAST_NAME</option>
                <option>STUDENT_ID</option>
            </select>
            <label for='studentid'>
                Student ID:
            </label>
            <input type='text' size='15' id='studentid' disabled='true'/><input type='text' id='filterstudenttxt'>
            <div id='choosestudent'>
            </div>
            <input type='submit' id='submit' value='      Finish      ' />
        </div>
    </body>
</html>