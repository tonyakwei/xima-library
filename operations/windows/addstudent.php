<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
   "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html>
	<head>
		<title>Add A Student...</title>
		<link rel='stylesheet' href='../../css/addstudent.css' type='text/css'>
		<script src='../../js/jquery.js'></script>
		<script src='../../js/addstudent.js'></script>
	</head>
	<body>
    	<h1>Add a student...</h1>
		<div id='addstudentmain'>
			<form method='post' action='../../php/db/addstudent.php'>
				<label for='name'>First Name:<b id='needed'>*</b></label>
				<input type='text' id='firstname' name='firstname' />
				
				<label for='lastname'>Last Name:<b id='needed'>*</b></label>
				<input type='text' id='lastname' name='lastname' />
				
				<label for='formnum'>Form Number:<b id='needed'>*</b></label>
				<select id='formnum' name='formnum'>
					<option>1</option>
					<option>2</option>
					<option>3</option>
					<option>4</option>
					<option>5</option>
					<option>L6</option>
					<option>U6</option>
				</select>
				
				<label for='formdiv'>Form Division:<b id='needed'>*</b></label>
				<select id='formdiv' name='formdiv'>
					<option>East</option>
					<option>West</option>
					<option> - </option>
				</select>
				<label for='notes'>Optional Notes:</label>
				<textarea id='notes' name='notes' cols='30' rows='8'></textarea><br>
				<input type='submit' id='submit' name='submit' value='   Add Student   '>
			</form>
			<div id='phpholder'>
				
			</div>
		</div>
		<div id='bookicon'>
        	<img src='../../img/userlarge.png' />
        </div>
	</body>
</html>