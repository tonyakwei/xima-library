<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
   "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <title>Manage Books...</title>
        <link rel='stylesheet' href='../../css/managebooks.css' type='text/css' />
		<link rel='stylesheet' href='../../css/viewnote.css' type='text/css' />
        <link rel='stylesheet' href='../../css/search.css' type='text/css' />
        <link rel='stylesheet' href='../../css/toolbar.css' type='text/css' />
        <link rel='stylesheet' href='../../css/tables/skyblue.css' type='text/css' />
        <script src='../../js/jquery.js'></script>
        <script src='../../js/managebooks.js'></script>
    </head>
    <body>
    	<div id='viewnotewrapper'>
    		<div id='darklayer'>
    			
    		</div>
    		<div id='viewnote'>
    			<h2>View Notes...</h2>
    			<textarea id='viewnotetxt' readonly='true'></textarea>
				<input type='button' value='Close' id='closebtn'/>
    		</div>
		</div>
        <div id='toolbar'>
            <ul>
                <li>
                    <a href='../../index.php'><img src='../../img/home.png'>Home</a>
                </li>
                <li>
                    <a href='#' class='floatright'><img src='../../img/help.png'>Help</a>
                </li>
            </ul>
        </div>
        <div id='search'>
            <h1>Filter...</h1>
            <br>
            <select id='searchcategory'>
                <option>TITLE</option>
                <option>AUTHOR</option>
                <option>GENRE1</option>
                <option>UNIQUE_ID</option>
            </select>
            <input type='text' id='filtertxt' name='filtertxt' size='80'/>
        </div>
        <div id='contents'>
            <br>
            <br>
			<div id='resultsHolder'>
	            <div id='booksresults'>
	            </div>
			</div>
        </div>
    </body>
</html>
