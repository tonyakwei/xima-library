<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
   "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html>
	<head>
		<title>Add A Book...</title>
		<link rel='stylesheet' href='../../css/addbook.css' type='text/css'>
		<script src='../../js/jquery.js'></script>
		<script src='../../js/addbook.js'></script>
	</head>
	<body>
    	<h1>Add a book...</h1>
		<div id='addbookmain'>
			<form method='post' action='../../php/db/addbook.php'>
				<label for='title'>Title: <b id='needed'>*</b></label>
				<input type='text' name='title' id='title'/>
				
				<label for='title'>Author :<b id='needed'>*</b></label>
				<input type='text' name='author' id='author'/>
				
				<label for='quality'>Quality: <b id='needed'>*</b></label>
				<select name='quality' id='quality'>
					<?php
						$filename = "../../php/selects/quality/options.txt";
						$qualityOptions = fopen($filename, 'r');
						rewind($qualityOptions);
						$text = fread($qualityOptions, filesize($filename));
						echo $text;
						fclose($qualityOptions);
					?>
				</select>
				<label for='genre1'>Genre 1:</label>
				<select name='genre1' id='genre1'>
					<option></option>
					<?php
						$filename = "../../php/selects/genre/options.txt";
						$qualityOptions = fopen($filename, 'r');
						rewind($qualityOptions);
						$text = fread($qualityOptions, filesize($filename));
						echo $text;
						fclose($qualityOptions);
					?>
				</select>
				<label for='genre2'>Genre 2:</label>
				<select name='genre2' id='genre2'>
					<option></option>
					<?php
						$filename = "../../php/selects/genre/options.txt";
						$qualityOptions = fopen($filename, 'r');
						rewind($qualityOptions);
						$text = fread($qualityOptions, filesize($filename));
						echo $text;
						fclose($qualityOptions);
					?>
				</select>
				
				<label for='dew1'>Dewey Number:</label>
				<input type='text' name='dew1' id='dew1' size='6'/>
				
				<label for='dew2'>Dewey Alpha:</label>
				<input type='text' name='dew2' id='dew2' size='6'/>
				
				<label for='isbn'>ISBN:</label>
				<input type='isbn' name='isbn' id='isbn'/>
				
				<label for='pages'>Number of Pages:</label>
				<input type='text' name='pages' id='pages'/>
				
				<label for='notes'>Optional Notes:</label>
				<textarea id='notes' name='notes' cols='30'  rows='4'></textarea>
				
				<br>
				
				<input type='submit' value='   Add Book!   ' name='submit' id='submit'/>
			</form>
			<div id='phpholder'>
				
			</div>
		</div>
		<div id='bookicon'>
        	<img src='../../img/books.png' />
        </div>
	</body>
</html>