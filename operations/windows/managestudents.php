<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
   "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <title>Manage Students...</title>
        <link rel='stylesheet' href='../../css/managebooks.css' type='text/css' />
		<link rel='stylesheet' href='../../css/viewnote.css' type='text/css' />
        <link rel='stylesheet' href='../../css/search.css' type='text/css' />
        <link rel='stylesheet' href='../../css/toolbar.css' type='text/css' />
        <link rel='stylesheet' href='../../css/tables/skyblue.css' type='text/css' />
		<link rel='stylesheet' href='../../css/editrecord.css' type='text/css' />
        <script src='../../js/jquery.js'></script>
        <script src='../../js/managestudents.js'></script>
    </head>
    <body>
    	<div id='viewnotewrapper'>
    		<div id='darklayer'>
    			
    		</div>
    		<div id='viewnote'>
    			<h2>View Notes...</h2>
    			<textarea id='viewnotetxt' readonly='true'></textarea>
				<input type='button' value='Close' id='closebtn'/>
    		</div>
		</div>
		<div id='shadelayer'>
    	
		</div>
        <div id='editobjectwrapper'>
            <div id='editobject'>
                <h2>Edit Record...</h2>
                <form method='post' action='../../php/db/getobject.php'>
                    <label for='editFName'>
                        First Name:
                    </label>
                    <input id='editFName' class='textClass'>
                    <label for='editLName'>
                        Last Name:
                    </label>
                    <input id='editLName' class='textClass'>
                    <label for='editFormNum'>
                        Form Number:
                    </label>
                    <select id='editFormNum'>
                        <option>1</option>
                        <option>2</option>
                        <option>3</option>
                        <option>4</option>
                        <option>5</option>
                        <option>6</option>
                    </select>
                    <label id='editFormDivision'>
                        Form Division:
                    </label>
                    <select id='editFormDivision'>
                        <option>East</option>
                        <option>West</option>
                    </select>
                    <label id='editNotes'>
                        Notes:
                    </label>
                    <textarea id='editNotes'>
                    </textarea>
                    <input type='submit' value='Commit' id='submit'/>
                </form>
            </div>
        </div>
        <div id='toolbar'>
            <ul>
                <li>
                    <a href='../../index.php'><img src='../../img/home.png'>Home</a>
                </li>
                <li>
                    <a href='#' class='floatright'><img src='../../img/help.png'>Help</a>
                </li>
            </ul>
        </div>
        <div id='search'>
            <h1>Filter...</h1>
            <br>
            <select id='searchcategory'>
                <option>FIRST_NAME</option>
                <option>LAST_NAME</option>
                <option>FORM_NUM</option>
                <option>FORM_DIV</option>
            </select>
            <input type='text' id='filtertxt' name='filtertxt' size='80'/>
        </div>
        <div id='contents'>
            <br>
            <br>
			<div id='resultsHolder'>
            	<div id='booksresults'>
            	</div>
            </div>
        </div>
    </body>
</html>
