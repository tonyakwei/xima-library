<?php
//Init of variables and connection to database
$connection  = mysql_connect('localhost','root','');
mysql_select_db('ximalibrary11');
$bookID = $_POST['bookID'];
$studentID = $_POST['studentID'];
$unknownError = "An unknown error has occured. The book may not have been successfully borrowed.";
$gotStudentName;
$gotBookTitle;
//Getting names of books and studnets for easier access later on
$resultsForStudentName = mysql_query("SELECT * FROM students WHERE STUDENT_ID='$studentID'");
while ($row = mysql_fetch_array($resultsForStudentName)) {
	$studentName = $row['FIRST_NAME'] . " " . $row['LAST_NAME'];
	if($row['CURRENT_BOOK'] != "None"){
		die("It seems that ".$row['FIRST_NAME']." already has a book out. The book's title is: \n".$row['CURRENT_BOOK']);
	}
	$gotStudentName = true;
}
$resultsForBookname = mysql_query("SELECT * FROM books WHERE UNIQUE_ID='$bookID'");
while($row = mysql_fetch_array($resultsForBookname)){
	$bookName = $row['TITLE'];
	if($row['STATUS'] == "OUT"){
		die("This book has already been borrowed. The current owner is: \n".$row['CURRENT_OWNER']);
	}
	$gotBookTitle = true;
}
if(!$gotBookTitle || !$gotStudentName){
	die("Either the book or the student does not exist; therfore the book could not be borrowed.");
}

//These are all of the String mysql_queries

$changeToOut = "UPDATE books SET STATUS='OUT' WHERE UNIQUE_ID='$bookID'";
$changeOwner = "UPDATE books SET CURRENT_OWNER='$studentName' WHERE UNIQUE_ID='$bookID'";
$changeBook = "UPDATE students SET CURRENT_BOOK='$bookName' WHERE STUDENT_ID='$studentID'";
//Of the queries are being executed

mysql_query($changeToOut) or die("An error has occured. ERROR CODE B1 - The book's status could not be changed to out.");
mysql_query($changeOwner) or die("An error has occured. ERROR CODE B2 - The owner of the book could not be changed.");
mysql_query($changeBook) or die("An error has occured. ERROR CODE B3 - The student's status could not be changed to indicate that he/she holds the book.");

echo "The book was successfully borrowed.";
mysql_close($connection);
?>