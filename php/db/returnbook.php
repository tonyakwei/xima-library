<?php
//Init of variables and connection to database
$connection  = mysql_connect('localhost','root','');
mysql_select_db('ximalibrary11');
$bookID = $_POST['bookID'];
$studentID = $_POST['studentID'];
$unknownError = "An unknown error has occured. The book may not have been successfully returned.";
$studentName;
$gotStudentName;
$gotBookTitle;
$bookName;

//Getting names of books and students for easier access later on
$resultsForStudentName = mysql_query("SELECT * FROM students WHERE STUDENT_ID='$studentID'");
while ($row = mysql_fetch_array($resultsForStudentName)) {
	$studentName = $row['FIRST_NAME'] . " " . $row['LAST_NAME'];
	if($row['CURRENT_BOOK'] == "None"){
		die("$studentName does not have a book out to return.");
	}
	$gotStudentName = true;
}

$resultsForBookname = mysql_query("SELECT * FROM books WHERE UNIQUE_ID='$bookID'");
while($row = mysql_fetch_array($resultsForBookname)){
	$bookName = $row['TITLE'];
	if($row['STATUS'] == "IN"){
		die("This book is already in the library.");
	}
	if($row['CURRENT_OWNER'] != $studentName){
		die("This student does not have this book.");
	}
	$gotBookTitle = true;
}
if(!$gotBookTitle || !$gotStudentName){
	die("Either the book or the student does not exist; therfore the book could not be returned.");
}

mysql_query("UPDATE students SET CURRENT_BOOK='None' WHERE STUDENT_ID='$studentID'") or die("An error has occured. ERROR CODE R1 - The student's current book could not be changed to 'none'.");
mysql_query("UPDATE books SET STATUS='IN' WHERE UNIQUE_ID='$bookID'") or die("An error has occured. ERROR CODE R2 - The book's status could not be changed to in.");
mysql_query("UPDATE books SET CURRENT_OWNER='No one' WHERE UNIQUE_ID='$bookID'") or die("An error has occured. ERROR CODE R3 - The book's owner could not be chagned to 'No one'.");

echo "The book was successfully returned.";
mysql_close($connection);
?>